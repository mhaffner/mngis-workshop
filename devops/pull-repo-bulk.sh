#!/bin/bash

# get list of usernames
users="$(ls /home)"

# get into root's copy of repo and pull changes; shouldn't need to reset
# anything here
cd /root/mngis-workshop
git pull

# loop through users
for i in ${users[*]}; do
    # check if directory exists (will only be necessary for the first time)
    if [ -d /home/"${i}"/git-repos/mngis-workshop ]; then
        rm -rf /home/"${i}"/git-repos/mngis-workshop
    fi

    # create git-repos directory if it doesn't exist
    if [ ! -d /home/"${i}"/git-repos/ ]; then
        mkdir /home/"${i}"/git-repos/
    fi

    # copy repo from root directory
    cp -R /root/mngis-workshop /home/"${i}"/git-repos/mngis-workshop

    # change ownership so users can read/write
    chown -R "${i}":"${i}" /home/"${i}"/git-repos/mngis-workshop
done
