library(rayshader)
library(raster)

dem <- raster("https://gitlab.com/mhaffner/data/-/raw/master/dem_01.tif")

## convert dem to matrix
dem_mat <- raster_to_matrix(dem)

## use one of rayshader's built-in textures
dem_mat %>%
  sphere_shade(texture = "desert") %>%
  plot_map()

dem_mat %>%
  sphere_shade(texture = "desert") %>%
  add_water(detect_water(dem_mat), color = "desert") %>%
  plot_map()

dem_mat %>%
  sphere_shade(texture = "desert") %>%
  add_water(detect_water(dem_mat), color = "desert") %>%
  add_shadow(ray_shade(dem_mat)) %>%
  plot_map()

dem_mat %>%
  sphere_shade(texture = "desert") %>%
  add_water(detect_water(dem_mat), color = "desert") %>%
  add_shadow(ray_shade(dem_mat)) %>%
  plot_3d(dem_mat, zscale = 10, fov = 0, theta = 135, zoom = 0.75, phi = 45, windowsize = c(1000, 800))

dem_mat %>%
  sphere_shade(texture = "desert") %>%
  add_water(detect_water(dem_mat), color = "desert") %>%
  add_shadow(ray_shade(dem_mat, zscale = 3), 0.5) %>%
  add_shadow(ambient_shade(dem_mat), 0) %>%
  plot_3d(dem_mat, zscale = 10, fov = 0, theta = 135, zoom = 0.75, phi = 45, windowsize = c(1000, 800))
Sys.sleep(0.2)
render_snapshot()

elmat %>%
  sphere_shade(texture = "desert") %>%
  add_water(detect_water(elmat), color = "desert") %>%
  add_shadow(ray_shade(elmat, zscale = 3), 0.5) %>%
  add_shadow(ambient_shade(elmat), 0) %>%
  plot_3d(elmat, zscale = 10, fov = 0, theta = 135, zoom = 0.75, phi = 45, windowsize = c(1000, 800))
Sys.sleep(0.2)
render_snapshot()
