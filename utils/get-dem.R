library(here)
library(raster)
library(tigris)
library(dplyr)
library(sf)
library(rayshader)
library(tmap)

#mn_dem <- raster(here("data/mn_dem.ovr"))
mn_dem <- raster(here("data/mn_dem"))
mn_dem <- raster("/home/matt/Downloads/mn_dem_3.tif")

stearns_co <- counties(state = "MN", cb = TRUE) %>%
  dplyr::filter(NAME == "Stearns") %>%
  st_transform(st_crs(mn_dem))

stearns_dem <- crop(mn_dem, stearns_co)

## crop to just the fix 500 rows and first 500 columns
stearns_dem_tmp <- crop(stearns_dem, extent(stearns_dem, 500,1000,500,1000))

el_mat <- raster_to_matrix(stearns_dem_tmp)

el_mat %>%
  sphere_shade(texture = "imhof1") %>%
  add_water(detect_water(el_mat)) %>%
  plot_map()

el_mat %>%
  sphere_shade(texture = "imhof1") %>%
  add_water(detect_water(el_mat), color = "imhof1") %>%
  add_shadow(ray_shade(el_mat, zscale = 3), 0.5) %>%
  add_shadow(ambient_shade(el_mat), 0) %>%
  plot_3d(el_mat, zscale = 5, fov = 0, theta = 135, zoom = 0.75, phi = 45, windowsize = c(1000, 800))
Sys.sleep(0.2)

tmap_mode("view")
qtm(stearns_dem_tmp)
