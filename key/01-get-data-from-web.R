## load packages here
library(sf)
library(tigris)
library(rnoaa)
library(dplyr)

## get tornado data from noaa
us_torn <- tornadoes() %>%
  st_as_sf() %>%
  st_transform(2163)

## alternatively, get them from a web source using the following lines:
##us_torn <- st_read("https://gitlab.com/mhaffner/data/-/raw/master/torn-lines.geojson") %>%
##  st_transform(2163)

us_torn

plot(us_torn$geometry)

## get counties data from tiger files
mn_counties <- counties(state = "MN", cb = TRUE) %>%
  st_transform(2163)

mn_counties

plot(mn_counties$geometry)

## count number of tornadoes in each county
mn_counties$torn <- st_intersects(mn_counties, us_torn) %>%
  lengths()

mn_counties

mn_counties$torn

plot(mn_counties["torn"])

## get just tornadoes in mn
mn_torn <- st_intersection(us_torn, mn_counties)

plot(mn_torn["mag"])
